sudo apt-get install build-essential cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev git -qy
cd build
cmake ../
make
make test
sudo make install
sudo srsran_install_configs.sh user
sudo ldconfig